import React from 'react';
import './Password1.css';
import {Grid, TextField, Button} from '@material-ui/core';
import ConfirmSaveModalPopup from './ConfirmSaveModalPopup';
import logoimg from '../../images/3-Logo Orquesti fondo claro.svg';


function Password1() {
	return(
		<>
			<Grid container className="container pw-content-main-section">
				<Grid item xs={12} className="pw-main-section-content">
					<Grid item xs={4} className="pw-empty-div"></Grid>
					<Grid item xs={4} className="pw-content-div">
						<div className="pw-logo-image-section">
							<img src={logoimg} alt=""/>
						</div>
						<h2 className="pw-revisa-head-text">Revisa tu correo</h2>
						<p className="pw-revisa-sub-text">Te hemos enviado una nueva contraseña a tu correo electrónico para que ingreses de nuevo a Orquesti</p>
						<form className="root pw-profile-form-section" noValidate autoComplete="off">
            				<TextField id="standard-basic" label="Contraseña Temporal" placeholder="" defaultValue=""/>
            				<TextField id="standard-basic" label="Nueva Contraseña" placeholder="" defaultValue=""/>
            				<TextField id="standard-basic" label="Confirmar Nueva Contraseña" placeholder="" defaultValue=""/>
            				<div className="pw-accordion-bottom-btn-sec">
            					<ConfirmSaveModalPopup/>
            				</div>
            				<p className="pw-bottom-text">¡No he recibido la contraseña!</p>
            			</form>
					</Grid>
					<Grid item xs={4} className="pr-empty-div"></Grid>
				</Grid>
			</Grid>	
		</>
	)
}
export default Password1;