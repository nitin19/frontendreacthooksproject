import React from 'react';
import './Amainroom.css';
import AMaccordion from './AMaccordion';
import {Grid} from '@material-ui/core';

function Amainroom(){
	return(
		<Grid container className="container am-content-main-section">
			<Grid item xs={12} className="am-main-section-content">
				<Grid item xs={4} className="am-empty-div"></Grid>
				<Grid item xs={4} className="am-content-div">
					<AMaccordion/>
				</Grid>
				<Grid item xs={4} className="am-empty-div"></Grid>
			</Grid>
		</Grid>
	)
}
export default Amainroom;