import React from 'react';
import './AMaccordion.css';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  makeStyles,
  Button,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

function AMaccordion(){
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [firstMainText,setFirstMainText]=React.useState('¡Hola, Andrea!');
  const [firstSubText,setFirstSubText]=React.useState('Verifica tus turnos para el día de hoy:');
  const [firstButtonText,setFirstButtonText]=React.useState('Empezar Turno');
  const [color,setColor]=React.useState('#549700');
  const [buttoncount,setButtonCount]=React.useState(0);
  const [expandcount,setExpandCount]=React.useState(0);
  const [disabledAccordion,setDisabledAccordion]=React.useState(false);
  const [expandColor,setExpandColor]=React.useState('#03219F');

  const handleChange = panel => (event, isExpanded) => {
      setExpanded(isExpanded ? panel : false);
  }; 
  const expandTextChange = () =>{
    setFirstMainText("¡Comencemos!");
    setFirstSubText("Detalles del Turno:");
    setExpandCount(expandcount+1);
    setExpandColor('#549700');
  }
  const buttonClickTextChange = () =>{
    setFirstMainText("Has comenzado un turno");
    setFirstSubText("¡Que tengas un buen día productivo!");
    setFirstButtonText("Finalizar turno");
    setColor("#BF0F3E");
    setButtonCount(buttoncount+1);
  }
  const RedButtonClick = () =>{
      setDisabledAccordion(true);
      setFirstMainText("¡Hola, Andrea!");
      setFirstSubText("Verifica tus turnos para el día de hoy:");
      setExpanded(true);
  }
  return(
    <>
      <h2 className="am-username-main-heading">{firstMainText}</h2>
      <p className="am-username-sub-heading">{firstSubText}</p> 
      <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        {expandcount==2 ?
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header" onClick={expandTextChange} style={{backgroundColor:expandColor}}>
            <Typography className={classes.heading}>
              <h2 className="am-accordion-timimg-head">8:00 am- 10:30 am</h2>
              <p className="am-accordion-timimg-sub-head">Walmart Curridabat</p>
            </Typography>
          </ExpansionPanelSummary>
          :
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header" onClick={expandTextChange} disabled={disabledAccordion}>
            <Typography className={classes.heading}>
              <h2 className="am-accordion-timimg-head">8:00 am- 10:30 am</h2>
              <p className="am-accordion-timimg-sub-head">Walmart Curridabat</p>
            </Typography>
          </ExpansionPanelSummary>
        }
        <ExpansionPanelDetails>
            <Typography>
              <p><strong>Ubicación:</strong><span>1 km este de Bomba la Galera carretera a Tres Ríos</span></p>
              <p><strong>Teléfono:</strong> <span>2272-1994</span></p>
              <p>
                <strong>Tareas:</strong>
                <ul className="am-accordion-content-points-sec">
                  <li>Limpieza de Estantes de la Marca</li>
                  <li>Inventario de Productos Maxfactor</li>
                  <li>Distribución de Regalías</li>
                </ul>
              </p>
              <p className="am-accordion-observaciones-sec"><strong>Observaciones: </strong><span className="am-accordion-edit-icon"><EditIcon/></span> </p>
              {buttoncount==1 ?
                <div className="am-accordion-bottom-btn-sec"><a href="javascript:0" className="am-accordion-content-btn-sec" onClick={RedButtonClick} style={{backgroundColor:color}}>{firstButtonText}</a></div>
                :
                <div className="am-accordion-bottom-btn-sec"><a href="javascript:0" className="am-accordion-content-btn-sec" onClick={buttonClickTextChange} style={{backgroundColor:color}}>{firstButtonText}</a></div>
              }
              {/*<div className="am-accordion-bottom-btn-sec"><a href="javascript:0" className="am-accordion-content-btn-sec" onClick={buttonClickTextChange} style={{backgroundColor:color}}>{firstButtonText}</a></div>*/}
            </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
            <Typography className={classes.heading}>
              <h2 className="am-accordion-timimg-head">8:00 am- 10:30 am</h2>
              <p className="am-accordion-timimg-sub-head">Walmart Curridabat</p>
            </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
            <Typography>
              Nulla facilisi. Phasellus sollicitudin nulla et quam mattis feugiat. Aliquam eget maximus est, id dignissim quam.
            </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </>
  )
}
export default AMaccordion;
