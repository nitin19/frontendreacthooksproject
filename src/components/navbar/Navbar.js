import React from 'react';
import './Navbar.css';
import { scaleDown as Menu } from 'react-burger-menu';
import logoimg from '../../images/3-Logo Orquesti fondo claro.svg';
import profileimg from '../../images/Profile.jpg';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import EventAvailableOutlinedIcon from '@material-ui/icons/EventAvailableOutlined';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

/*const showSettings = (event) => {
    event.preventDefault();
}*/
function Navbar(props){
	return(
		<div className="nav-main-section">
			<div className="nav-logo-section">
				<img src={logoimg} alt=""/>
			</div>
	        <Menu right>
	        	<div className="nav-menu-logo-section">
	        		<img src={logoimg} alt=""/>
	        	</div>
	        	<div className="nav-menu-profile-img-section">
	        		<img src={profileimg} alt=""/>
	        	</div>
	        	<h2 className="nav-menu-user-name-text">Andreas Preis</h2>
	    		<a id="home" className="menu-item" href="/"><HomeOutlinedIcon/> Inicio</a>
	    		<a id="about" className="menu-item" href="/about"><EventAvailableOutlinedIcon/>Calendario</a>
	    		<a id="contact" className="menu-item" href="/contact"><AccountCircleOutlinedIcon/>Perfil</a>
	    		<a id="logout" className="menu-item" href="/logout"><ExitToAppIcon/>Cerrar sesión</a>
	    		{/*<a onClick={showSettings } className="menu-item--small" href="">Settings</a>*/}
	  		</Menu>
  		</div>
    )
}
export default Navbar;