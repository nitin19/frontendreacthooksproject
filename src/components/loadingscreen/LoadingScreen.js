import React from 'react';
import './LoadingScreen.css';
import {Grid} from '@material-ui/core';
import logoimg from '../../images/2-Logo Orquesti monocromático.svg';

function LoadingScreen() {
  return (
    <div>
      <Grid container className="container ls-main-section">
        <img src={logoimg} alt=""/>
      </Grid>
    </div>
  )
}
export default LoadingScreen;