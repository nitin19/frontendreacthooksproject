import React from 'react';
import clsx from 'clsx';
import './login.css';
import {
  Grid, 
  TextField, 
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
} from '@material-ui/core';
import leftimg from '../../images/newBitmap.png';
import { makeStyles } from '@material-ui/core/styles';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1),
  },
}));
function Login() {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });
  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={8} className="lg-left-section">
          <Grid item xs={6}>
            <h2 className="lg-sub-sec-heading">Bienvenido a Orquesti</h2>
            <p className="lg-sub-sec-sub-heading">Horarios bajo control</p>
          </Grid>
          <Grid item xs={6}>
            <img className="lg-sub-sec-right-section-img" src={leftimg} alt="xxc" />
          </Grid>
        </Grid>
        <Grid item xs={4} className="lg-right-section">
          <h2 className="lg-right-form-heading">Inicia Sesión</h2>
          <form className="root lg-login-form-section" noValidate autoComplete="off">
            <TextField id="standard-basic" label="andreaspreis2@gmail.com" />
            <FormControl className={clsx(classes.margin, classes.textField)}>
              <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
              <Input
                id="standard-adornment-password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                onChange={handleChange('password')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {values.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </form>
          <p className="lg-right-section-signup-link-text">Olvidé mi contraseña</p>
        </Grid>
      </Grid>
    </div>
  )
}

export default Login;