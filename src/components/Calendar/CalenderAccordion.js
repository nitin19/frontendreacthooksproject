import React from 'react';
import './CalenderAccordion.css';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  makeStyles,
  Button,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));
function CalenderAccordion(){
	const classes = useStyles();
  	const [expanded, setExpanded] = React.useState(false);
  	//const [firstMainText,setFirstMainText]=React.useState('¡Hola, Andrea!');
  	const [firstSubText,setFirstSubText]=React.useState('Selecciona un día para conocer tus turnos:');
  	/*const [firstButtonText,setFirstButtonText]=React.useState('Empezar Turno');*/
  	const [color,setColor]=React.useState('#1A73E8');
  	const [newdate,setNewDate]=React.useState('');

  	const handleChange = panel => (event, isExpanded) => {
      	setExpanded(isExpanded ? panel : false);
  	}; 
  	const expandTextChange = () =>{
    	//setFirstMainText("¡Comencemos!");
    	setFirstSubText("Selecciona un día para ver el detalle de tus turnos");
    	setColor("#03229F");
  	}
  	const weekdayitems = [
	    {
	      id: '1',
	      weekdayname: 'Lun',
	    },
	    {
	      id: '2',
	      weekdayname: 'Mar',
	    },
	  	{
	      id: '3',
	      weekdayname: 'Mie',
	  	},
	  	{
	      id: '4',
	      weekdayname: 'Jue',
	  	},
	  	{
	      id: '5',
	      weekdayname: 'Vie',
	  	},
	  	{
	      id: '6',
	      weekdayname: 'Sab',
	  	},
	  	{
	      id: '7',
	      weekdayname: 'Dom',
	  	}
  	]
  	const dateitems = [
	    {
	      id: '1',
	      date: '1',
	    },
	    {
	      id: '2',
	      date: '2',
	    },
	  	{
	      id: '3',
	      date: '3',
	  	},
	  	{
	      id: '4',
	      date: '4',
	  	},
	  	{
	      id: '5',
	      date: '5',
	  	},
	  	{
	      id: '6',
	      date: '6',
	  	},
	  	{
	      id: '7',
	      date: '7',
	  	}
  	]
  	return(
    	<>  <h2 className="ca-revisa-head-text">Semana del 1 al 7 de Febrero</h2>
			<p className="ca-revisa-sub-text">{firstSubText}</p>
			<ul className="ca-calender-weekday-text">
				{weekdayitems.map(item =>(
					<li>{item.weekdayname}</li>
				))}
			</ul>
			<ul className="ca-calender-date-text">
				{dateitems.map(item =>(
					<li onClick={() => setNewDate(item.date)}>{item.date}</li>
				))}
			</ul>
			<h2 className="ca-select-date-text-head">Lunes {newdate} de Febrero, 2020</h2>
      		<ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        		<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" className="ca-accordion-expand-section" id="panel1bh-header" style={{backgroundColor:color}} onClick={expandTextChange}>
            		<Typography className={classes.heading}>
              			<h2 className="ca-accordion-timimg-head">8:00 am- 10:30 am</h2>
              			<p className="ca-accordion-timimg-sub-head">Walmart Curridabat</p>
            		</Typography>
        		</ExpansionPanelSummary>
        		<ExpansionPanelDetails>
            		<Typography>
              			<p><strong>Ubicación:</strong><span>1 km este de Bomba la Galera carretera a Tres Ríos</span></p>
              			<p><strong>Teléfono:</strong> <span>2272-1994</span></p>
              			<p>
                			<strong>Tareas:</strong>
                			<ul className="ca-accordion-content-points-sec">
                  				<li>Limpieza de Estantes de la Marca</li>
                  				<li>Inventario de Productos Maxfactor</li>
                  				<li>Distribución de Regalías</li>
                			</ul>
              			</p>
              			<p className="ca-accordion-observaciones-sec"><strong>Observaciones: </strong><span className="ca-accordion-edit-icon"><EditIcon/></span> </p>
              			{/*<div className="ca-accordion-bottom-btn-sec"><Button variant="contained" color="secondary">Empezar Turno</Button></div>*/}
            		</Typography>
        		</ExpansionPanelDetails>
      		</ExpansionPanel>
      		<ExpansionPanel expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        		<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" className="ca-accordion-expand-section" id="panel2bh-header" style={{backgroundColor:color}} onClick={expandTextChange}>
            		<Typography className={classes.heading}>
              			<h2 className="ca-accordion-timimg-head">1:00 pm- 4:30 pm</h2>
              			<p className="ca-accordion-timimg-sub-head">Walmart Escazú</p>
            		</Typography>
        		</ExpansionPanelSummary>
        		<ExpansionPanelDetails>
            		<Typography>
              			Nulla facilisi. Phasellus sollicitudin nulla et quam mattis feugiat. Aliquam eget maximus est, id dignissim quam.
            		</Typography>
        		</ExpansionPanelDetails>
      		</ExpansionPanel>
    	</>
  	)
}
export default CalenderAccordion;