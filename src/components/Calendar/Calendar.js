import React from 'react';
import './Calendar.css';
import {Grid, TextField, Button} from '@material-ui/core';
import CalendarAccordion from './CalenderAccordion';
import logoimg from '../../images/3-Logo Orquesti fondo claro.svg';

function Calendar() {
	return(
		<Grid container className="container ca-content-main-section">
			<Grid item xs={12} className="ca-main-section-content">
				<Grid item xs={4} className="ca-empty-div"></Grid>
				<Grid item xs={4} className="ca-content-div">
					<h2 className="ca-page-title-head">Calendario</h2>
					<CalendarAccordion/>
				</Grid>
				<Grid item xs={4} className="ca-empty-div"></Grid>
			</Grid>
		</Grid>
	)
}
export default Calendar;