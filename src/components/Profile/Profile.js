import React from 'react';
import './Profile.css';
import {Grid, TextField, InputAdornment, IconButton} from '@material-ui/core';
import profileimg from '../../images/Profile.jpg';
import Edit from "@material-ui/icons/Edit";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: 50
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    color: "black",
    fontSize: 30,
    opacity: 1,
    borderBottom: 0,
    "&:before": {
      borderBottom: 0
    }
  },
  disabled: {
    color: "black",
    borderBottom: 0,
    "&:before": {
      borderBottom: 0
    }
  },
  btnIcons: {
    marginLeft: 10
  }
}));

function Profile(){
	const classes = useStyles();
	const [email, setEmail] = React.useState("johndoe@domain.com");
	const [editMode, setEditMode] = React.useState(false);
	const [mouseOver, setMouseOver] = React.useState(true);
	const handleChange = event => {
    	setEmail({ email: event.target.value });
  	};

	const handleMouseOver = event => {
	    if (!mouseOver) {
	      setMouseOver({ mouseOver: false });
	    }
	};

	const handleMouseOut = event => {
	    if (mouseOver) {
	      setMouseOver({ mouseOver: true });
	    }
	};

	const handleClick = () => {
	    setEditMode(true);
	    setMouseOver(true);
	};
	return(
		<>
			<Grid container className="container pr-content-main-section">
				<Grid item xs={12} className="pr-main-section-content">
					<Grid item xs={4} className="pr-empty-div"></Grid>
					<Grid item xs={4} className="pr-content-div">
						<div className="pr-profile-image-section">
							<img src={profileimg} alt=""/>
						</div>
						<h2 className="pr-username-text">¡Hola, Andreas!</h2>
						<form className="root pr-profile-form-section" noValidate autoComplete="off">
            				<TextField id="standard-basic" label="Nombre Completo" placeholder="Andreas Preis Ramírez"/>
            				<TextField id="standard-basic" label="Identificación" placeholder="ID288736405"/>
            				<TextField 
            					id="standard-basic" 
            					label="Número Telefónico" 
            					placeholder="+506 6546-9888"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
            				<TextField 
            					id="standard-basic" 
            					label="Ciudad" 
            					placeholder="Curridabat"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
            				<TextField id="standard-basic" label="Código Postal" placeholder="11800"/>
            				<TextField 
            					id="standard-basic" 
            					label="Provincia o Estado" 
            					placeholder="San José"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
            				<TextField 
            					id="standard-basic" 
            					label="País" 
            					placeholder="Costa Rica"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
            				<TextField 
            					id="standard-basic" 
            					label="Correo Electrónico" 
            					placeholder="andreaspreis@gmail.com"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
            				<TextField 
            					id="standard-basic"
            					type="password" 
            					label="Contraseña" 
            					placeholder="Password"
            					margin="normal"
            					onChange={handleChange}
          						disabled={!editMode}
          						className={classes.textField}
          						onMouseEnter={handleMouseOver}
          						onMouseLeave={handleMouseOut}
						        InputProps={{
						            classes: {
						              disabled: classes.disabled
						            },
						            endAdornment: mouseOver ? (
						              <InputAdornment position="end" className="edit-icon">
						                <IconButton onClick={handleClick}>
						                  <Edit />
						                </IconButton>
						              </InputAdornment>
						            ) : (
						              ""
						            )
						        }}
            				/>
          				</form>
					</Grid>
					<Grid item xs={4} className="pr-empty-div"></Grid>
				</Grid>
			</Grid>
		</>
	)
}
export default Profile;