import React, {Fragment} from 'react';
import './App.css';
import Login from './components/login/Login';
import LoadingScreen from './components/loadingscreen/LoadingScreen';
import Navbar from './components/navbar/Navbar';
import Amainroom from './components/Amainroom/Amainroom';
import Profile from './components/Profile/Profile';
import Password1 from './components/Password1/Password1';
import Calendar from './components/Calendar/Calendar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route exact path="/" component={LoadingScreen}/>
          <Route path="/login" component={Login}/>
          <Route path="/password1" component={Password1}/>
          <Fragment>
            <Navbar pageWrapId={"page-wrap"} outerContainerId={"App"}/>
            <Route path="/amainroom" component={Amainroom}/>
            <Route path="/profile" component={Profile}/>
            <Route path="/calendar" component={Calendar}/>
          </Fragment>
          
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
}

export default App;

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      containedPrimary: {
        color: 'white',
        boxShadow: 'none',
      },
      containedSecondary: {
        color: 'white',
        boxShadow: 'none',
      },
      root: {
        width: '150px',
        height: '50px',
        textTransform: 'none',
      },
    },
    MuiAppBar: {
      root: {
        height: '71px',
        width: 'calc(100% - 105px)',
        boxShadow: 'none',
        borderBottomWidth: '1px',
        borderBottom: 'solid',
        borderBottomColor: '#e3e3e3',
      },
      colorPrimary: {
        color: 'black',
        backgroundColor: 'white',
      }
    },
    MuiDrawer: {
      paper: {
        width: '105px',
        boxShadow: '0px 1px 8px #e3e3e3',
      },
    },
    MuiListItem: {
      root: {
        color: '#9498A9',
        '&$selected': {
          color: '#5A8DEE',
          backgroundColor: '#ECF2FF',
          borderRight: '5px solid #5A8DEE',
          '&:hover' : {
            backgroundColor: '#ECF2FF',
          },
        },
      },
      button: {
        '&:hover' : {
          backgroundColor: '#ECF2FF',
        }
      },
    },
  },
  typography: {
    fontFamily: [
      'Source Sans Pro',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    primary: { 
      main: '#5A8DEE',
    },
    secondary: {
      main: '#FAC56F',
    },
  },
});